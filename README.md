# tmsh

A collection of scripts and such to make life easier.

## Prereqs

I use the latest Ubuntu version so that's assumed for all of the scripts

## Installation

```
# will ask for sudo perms
./install.sh
```

## Usage

```
# flashes latest nightly release per specified platform, skips mpa config
voxl-flash-nightly -p|--platform {voxl2|voxl|rb5|voxl2-mini}
```
