#!/bin/bash

set -e

DOWNLOADS_DIR=~/Downloads/

# Function to display usage
usage() {
    echo "Usage: $0 -p|--platform {voxl2|voxl|rb5|voxl2-mini}"
    exit 1
}

# Parse arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -p|--platform) 
            platform="$2"
            shift 
            ;;
        *) 
            echo "Unknown parameter passed: $1"
            usage 
            ;;
    esac
    shift
done

# Check if platform argument was provided
if [ -z "$platform" ]; then
    echo "Error: --platform or -p argument is required."
    usage
fi

# Validate platform argument
case $platform in
    voxl2|voxl|rb5|voxl2-mini)
        echo "Platform '$platform' is valid."
        # Add your script logic here
        ;;
    *)
        echo "Error: Invalid platform '$platform'."
        usage
        ;;
esac

PLATFORM_IMAGE_FILE_URI=$(gsutil ls gs://platform-nightlies/$platform | sort -k2,2 | tail -n 1 | head -n 1)
PLATFORM_IMAGE_ID_TAR=$(basename $PLATFORM_IMAGE_FILE_URI)
PLATFORM_IMAGE_ID="${PLATFORM_IMAGE_ID_TAR%.tar.gz}"

echo "Downloading: ${PLATFORM_IMAGE_FILE_URI}"

if [ -d "${DOWNLOADS_DIR}/${PLATFORM_IMAGE_ID}" ]; then
	echo -e "\n[INFO] Found untarred release: $PLATFORM_IMAGE_ID in $DOWNLOADS_DIR"
elif [ -f ${DOWNLOADS_DIR}/${PLATFORM_IMAGE_ID_TAR} ]; then
	echo -e "\n[INFO] Found tar: $PLATFORM_IMAGE_ID_TAR in $DOWNLOADS_DIR"

	echo -e "\n[INFO] Untarring tar in $DOWNLOADS_DIR..."
	tar -xzf "${DOWNLOADS_DIR}/${PLATFORM_IMAGE_ID_TAR}" -C "${DOWNLOADS_DIR}"
else
	echo -e "\n[INFO] Release not found in $DOWNLOADS_DIR, downloading..."
	gsutil cp "${PLATFORM_IMAGE_FILE_URI}" "${DOWNLOADS_DIR}"
	
	echo -e "\n[INFO] Unzipping tar in cache..."
	tar -xzf "${DOWNLOADS_DIR}/${PLATFORM_IMAGE_ID_TAR}" -C "${DOWNLOADS_DIR}"
fi

cd $DOWNLOADS_DIR/$PLATFORM_IMAGE_ID

./install.sh -n -m
